<?php

// error displaying . . .
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

// we create our variables for making a connection . . .
$user = '';
$pass = '';
$db = '';
$host = 'localhost';

$pdo = new PDO("mysql:host=$host;dbname=$db", $user, $pass);

// connection tester . . .
// try {
    
//     if ($pdo) {
//         echo "Connected to the $db database successfully!";
//     }
// } catch (PDOException $e) {
//     print "Error!: " . $e->getMessage() . "<br/>";
//     die();
// }
