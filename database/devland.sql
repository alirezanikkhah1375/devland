-- phpMyAdmin SQL Dump
-- version 4.9.5deb2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Dec 17, 2021 at 04:07 PM
-- Server version: 8.0.27-0ubuntu0.20.04.1
-- PHP Version: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `devland`
--

-- --------------------------------------------------------

--
-- Table structure for table `clients`
--

CREATE TABLE `clients` (
  `clientID` int NOT NULL,
  `firstname` varchar(100) NOT NULL,
  `lastname` varchar(100) NOT NULL,
  `address` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `clients`
--

INSERT INTO `clients` (`clientID`, `firstname`, `lastname`, `address`) VALUES
(1, 'Alireza', 'NIKKHAH', '18 rue de newyork , Toulouse 31300'),
(2, 'Jeff', 'ABRAHAM', '145 rue de chicago , Toulouse 31000'),
(4, 'Daniel', 'MAKAKO', '45 route de soie , Paris 75001'),
(6, 'Marissa', 'RUSSELL', '12 avenue Gilles , Limoges 87100'),
(7, 'Eliana', 'SALAZAR', '84 Kent Smith Rd Pinehurst , Paris 75003'),
(8, 'Amanda', 'ALVARADO', '1424 E Oakwood Plant City , Bordeaux 33003'),
(9, 'Margaret', 'TRAN', '695 N Washington St Van Wert , Bordeaux 33103');

-- --------------------------------------------------------

--
-- Table structure for table `developers`
--

CREATE TABLE `developers` (
  `devID` int NOT NULL,
  `firstname` varchar(100) NOT NULL,
  `lastname` varchar(100) NOT NULL,
  `level` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `developers`
--

INSERT INTO `developers` (`devID`, `firstname`, `lastname`, `level`) VALUES
(1, 'Travis', 'SCOTT', 'Junior'),
(2, 'Lady', 'GAGA', 'Senior'),
(3, 'Michael', 'JACKSON', 'Tech lead'),
(4, 'Lionel', 'MESSI', 'Junior'),
(5, 'Thierry', 'HENRY', 'Senior'),
(6, 'Chris', 'MARTIN', 'Senior');

-- --------------------------------------------------------

--
-- Table structure for table `projects`
--

CREATE TABLE `projects` (
  `projectID` int NOT NULL,
  `name` varchar(200) NOT NULL,
  `description` text NOT NULL,
  `clientID` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `projects`
--

INSERT INTO `projects` (`projectID`, `name`, `description`, `clientID`) VALUES
(1, 'One-page layout', 'This project aims to recreate a pixel perfect design and make a one-page responsive layout. This is also a beginner-level project that allows freshers to test their newly acquired knowledge and skill level. You can use the Conquer template to build this project. This template comes loaded with a host of unique layouts. Also, it brings before you a series of challenges that Web Developers often face in real-world scenarios. As a result, you are pushed to experiment with new technologies like Floats and Flexbox to hone the implementation of CSS layout techniques.', NULL),
(2, 'Login authentication', 'This is a beginner-level project that is great for honing your JavaScript skills. In this project, you will design a website’s login authentication bar – where users enter their email ID/username and password to log in to the site. Since almost every website now comes with a login authentication feature, learning this skill will come in handy in your future web projects and applications.', NULL),
(3, 'Product landing page', 'To develop a product landing page of a website, you must have sound knowledge of HTML and CSS. In this project, you will create columns and align the components of the landing page within the columns. You will have to perform basic editing tasks like cropping and resizing images, using design templates to make the layout more appealing, and so on. ', NULL),
(4, 'Modal pop-ups', 'This project is very similar to the social share button project. Here, you need to create a JavaScript code that will be immediately triggered every time a user clicks a button on the page or loads the page. \r\n\r\nYou will design modal pop-ups to provide notifications, promotions, and email signups to users. The pop-up should be such that it can be closed with a click as well. To make the project more challenging, you can experiment with different animations and modal entrances like fade-in and slide-out.', NULL),
(5, 'Giphy with a unique API', 'This web development project aims to create a JavaScript quiz game that can take multiple answers and show the correct result to users. While gaining JavaScript knowledge isn’t tricky, applying that knowledge in real-world scenarios is usually challenging. However, you can experiment with your skills by working on a small JavaScript-based quiz game. \n\nWhile building this project, you will not only deal with complex logic, but you will also learn a lot about data management and DOM manipulation. Depending on your JavaScript skills and ability to handle complex logic, you can make the game as simple or complicated as you want it to be!', NULL),
(6, 'To-do list', 'You can use JavaScript to build a web app that allows you to make to-do lists for routine tasks. For this project, you must be well-versed with HTML and CSS. JavaScript is the best choice for a to-do project since it allows users to design interactive coding lists where you can add, delete, and also group items. ', NULL),
(7, 'SEO-friendly website', 'Today, SEO is an integral part of website building. Without SEO, your website will not have the visibility to drive traffic from organic searches in SERPs (search engine result pages). While Web Developers are primarily concerned about the website functionality, they must have a basic idea of web design and SEO. In this project, you will take up the role of a Digital Marketer and gain in-depth knowledge of SEO. It will be helpful if you are aware of the technical SEO for this project.\n\nWhen you are well-versed in SEO, you can build a website having user-friendly URLs and featuring an integrated, responsive design. This will allow the site to load quickly on both desktop or mobile devices, thereby strengthening a brand’s social media presence.', NULL),
(8, 'JavaScript drawing', 'This project is inspired by Infinite Rainbow on CodePen. This JavaScript-based project uses JavaScript as a drawing tool to bring to life HTML and CSS elements on a web browser. The best thing about this project is that you can take advantage of JavaScript’s supercool drawing libraries like oCanvas, Canviz, Raphael, etc. \n\nBy working on this project, you can learn how to use and implement JavaScript’s drawing capabilities. This skill will come in handy for enhancing the appeal of static pages by adding graphical elements to them.', NULL),
(9, 'Search engine result page', 'This is a super interesting and exciting project! In this project, you have to create a search engine result page that resembles Google’s SERPs. While building this project, you must ensure that the webpage can display at least ten search results (just like Google). Also, you must include the navigation arrow at the bottom of the webpage so that users can switch to the next page.', NULL),
(10, 'Google home page lookalike', 'Another interesting JavaScript project on our list, this project requires you to build a webpage that resembles Google’s home page. Keep in mind that you have to build a replica of Google home page along with the Google logo, search icons, text box, Gmail, and images buttons – basically, everything that you see on Google’s home page. This should be relatively easy, provided you proficient in HTML and CSS.\n\nSince the aim here is to build a replica of Google’s home page, you need not worry too much about the functionality of the components of the page.', NULL),
(11, 'AJAX-style login', 'The focus of this project is to build the front-end of an AJAX-style login site/page. In AJAX-style login, the login page does not need to be reloaded to ensure whether or not you have to input the correct login details. \n\nIf you want, you can also create a mockup of both successful and invalid login situations by hard-coding a username and password and compare this to the information entered by a user. You can also include error messages for situations where the input data is incorrect or not found.', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `clients`
--
ALTER TABLE `clients`
  ADD PRIMARY KEY (`clientID`);

--
-- Indexes for table `developers`
--
ALTER TABLE `developers`
  ADD PRIMARY KEY (`devID`);

--
-- Indexes for table `projects`
--
ALTER TABLE `projects`
  ADD PRIMARY KEY (`projectID`),
  ADD KEY `fk_projects` (`clientID`) USING BTREE;

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `clients`
--
ALTER TABLE `clients`
  MODIFY `clientID` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `developers`
--
ALTER TABLE `developers`
  MODIFY `devID` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `projects`
--
ALTER TABLE `projects`
  MODIFY `projectID` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `projects`
--
ALTER TABLE `projects`
  ADD CONSTRAINT `fk_projects` FOREIGN KEY (`clientID`) REFERENCES `clients` (`clientID`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
