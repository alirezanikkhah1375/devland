<?php
require '../pages/connection.php';
require 'header.php';
if (!empty($_GET['id'])) {
    $id = $_GET['id'];
    $sql = "SELECT * FROM projects where projectID=$id";
    $query = $pdo->prepare($sql);
    $query->execute();
    $res = $query->fetchAll();
    foreach ($res as $row) {
        $projetName = $row['name'];
        $description = $row['description'];
    }
}
?>

<main>
    <article>
        <div class="container content">
            <h2><?= $projetName ?></h2>
            <p><?= $description ?></p>
            <div class="btn-space">
                <a href="index.php" class="return-btn">Retour</a>
            </div>
        </div>
    </article>
</main>

<?php
require 'footer.php';
?>