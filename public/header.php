<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <title>Gestion de Projets | Simplon.co</title>
</head>

<body>

    <header>
        <div class="container top">
            <img src="/images/MCD.png" alt="Simplon.co">
            <h1>Gestion de Projets</h1>
        </div>
    </header>