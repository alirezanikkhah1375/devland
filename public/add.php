<?php
require '../pages/connection.php';
if (isset($_POST['add'])) {
    $nameClient = $_POST['nameClient'];
    $name = $_POST['name'];
    $description = $_POST['description'];
    if (!empty($name) && !empty($description) && !empty($name)) {
        $sql = "INSERT INTO projects(name, description , clientID)
        VALUES ('$name', '$description', '$nameClient');";
        $qry = $pdo->query($sql);
        if ($qry) {
            header("location: index.php");
        } else {
            echo "une erreur survenue";
        }
    }
}
require 'header.php';
?>

<main>

    <div class="container add-page">
        <h2>Ajouter un nouveau projet</h2>
        <form method="POST">

            <label for="nameClient">nom du client</label>
            <select name="nameClient" id="nameClient">

                <option value="" hidden>veuillez choisir le nom du client</option>

                <?php

                // for having the name of clients in the select we need to get them from client table by a loop . . .
                $sql = "SELECT * FROM clients";
                $query = $pdo->prepare($sql);
                $query->execute();
                $res = $query->fetchAll();

                foreach ($res as $row) {
                    $clientFirstname = $row['firstname'];
                    $clientLastname = $row['lastname'];
                    $clientID = $row['clientID'];
                ?>
                    <!-- for printing the names as options and using their id as value for adding to the clientID column . . . -->
                    <option value="<?= $clientID ?>"><?= $clientFirstname . " " . $clientLastname ?></option>

                <?php } ?>

            </select>

            <!-- <input type="text" name="nameClient" id="nameClient" required> -->

            <label for="name">nom du projet</label>
            <input type="text" name="name" id="name" required>

            <label for="description">description du projet</label>
            <textarea name="description" id="description" cols="30" rows="10" required></textarea>

            <div class="ajouter">
                <a href="index.php" class="return-btn">Retour</a>
                <input type="submit" value="Ajouter" name="add" class="ajouter-btn">
            </div>

        </form>
    </div>

</main>

<?php
require 'footer.php';
?>