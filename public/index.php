<?php
require '../pages/connection.php';

// for deleting we use method get and use id sent to URL . . .
if (isset($_GET['id'])) {
    $urlID = $_GET['id'];
    $deleteSql = "DELETE FROM projects WHERE projectID =$urlID;";
    $query = $pdo->query($deleteSql);
    if ($query) {
        header("location: index.php");
    } else {
        echo "échec de la suppression";
    }
}
require 'header.php';
?>

<main>
    <div class="projets container">

        <h2>Projets</h2>

        <div class="add">
            <a href="add.php" class="btn-add">Ajouter un projet</a>

            <table>

                <tr>
                    <th>Projet</th>
                    <th>Action</th>
                </tr>

                <?php
                // gettin info with pdo from database for filling the table . . .
                $sql = "SELECT * FROM projects";
                $query = $pdo->prepare($sql);
                $query->execute();
                $res = $query->fetchAll();

                foreach ($res as $row) {
                    $projetName = $row['name'];
                    $id = $row['projectID'];
                ?>
                    <tr>
                        <th><?= "$projetName"; ?></th>
                        <th>
                            <div class="action-btn">
                                <a href="content.php?id=<?= $id ?>" class="see">Voir</a>
                                <a href="modify.php?id=<?= "$id" ?>" class="modify">Modifier</a>
                                <a href="index.php?id=<?= "$id" ?>" class="delete">Supprimer</a>
                            </div>
                        </th>
                    </tr>

                <?php
                }
                ?>

            </table>
        </div>
    </div>

</main>

<?php
require 'footer.php';
?>