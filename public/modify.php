<?php
require 'header.php';
require '../pages/connection.php';

// we will get info of the selected project and put it for edit in edit part.
if (!empty($_GET['id'])) {
    $urlID = $_GET['id']; // for getting the id in URL . . .
    $sql = "SELECT * FROM projects WHERE projectID = $urlID";
    $query = $pdo->prepare($sql); // for doing a query . . .
    $query->execute();
    $res = $query->fetch(); // for getting the info . . . it give in only one array if you use FetchAll it will  give an array of arrays.
    $name = $res['name'];
    $text = $res['description'];

    // My text are ready to be modified . . . let's gooooo!!!

    // we start to get the info posted by our from and do the changes . . .
    if (isset($_POST['modifier'])) {
        $newName = $_POST['name'];
        $newText = $_POST['description'];
        if (!empty($newName) && !empty($newText)) {
            $sql = "UPDATE projects SET name = '$newName', description = '$newText' WHERE projectID = $urlID;";
            $query = $pdo->query($sql);
            if ($query) {
                header("location: index.php");
            } else {
                echo "Une erreur est survenue (upadte)";
            }
        }
    }
}
?>

<main>

    <div class="container add-page">
        <h2>Modification</h2>
        <form method="POST">
            <label for="name">Le nom du projet</label>
            <input type="text" name="name" id="name" value="<?= $name ?>" required>

            <label for="description">La description du projet</label>
            <textarea name="description" id="description" cols="30" rows="10" required><?= $text ?></textarea>

            <div class="ajouter">
                <a href="index.php" class="return-btn">Retour</a>
                <input type="submit" value="Modifier" name="modifier" class="modifier-btn">
            </div>
        </form>
    </div>

</main>

<?php
require 'footer.php';
?>